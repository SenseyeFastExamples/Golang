package extension

import (
	"gitlab.com/SenseyeFastExamples/Golang/extension/entity"
	"gitlab.com/SenseyeFastExamples/Golang/extension/proto"
	"testing"
)

// BenchmarkEventInlineEntity-4             5000000               265 ns/op
func BenchmarkEventInlineEntity(b *testing.B) {
	items := make([]entity.Entity, 0, b.N)

	for i := 0; i < b.N; i++ {
		u := uint32(i)

		items = append(items, &proto.Event{
			Time:    u,
			Type:    u,
			Browser: u,
			Os:      u,
			Clicks:  u,
		})
	}

	b.ResetTimer()
	benchmark(items)
}

// BenchmarkEventExtractEntity-4            5000000               265 ns/op
func BenchmarkEventExtractEntity(b *testing.B) {
	items := make([]entity.Entity, 0, b.N)

	for i := 0; i < b.N; i++ {
		u := uint32(i)

		items = append(items, &entity.Event{&proto.Event{
			Time:    u,
			Type:    u,
			Browser: u,
			Os:      u,
			Clicks:  u,
		}})
	}

	b.ResetTimer()
	benchmark(items)
}

func benchmark(items []entity.Entity) {
	if len(items) == 0 {
		return
	}

	item := items[0]

	_ = item.Table()
	_ = item.Fields()

	for _, item := range items {
		_ = item.Values()
	}
}
