package proto

// like protobuf
type Event struct {
	Time    uint32
	Type    uint32
	Browser uint32
	Os      uint32
	Clicks  uint32
}
