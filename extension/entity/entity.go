package entity

type Entity interface {
	Table() string
	Fields() []string
	Values() []interface{}
}
