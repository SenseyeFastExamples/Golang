package entity

import "gitlab.com/SenseyeFastExamples/Golang/extension/proto"

var fields = []string{
	"time",
	"type",
	"browser",
	"os",
	"clicks",
}

type Event struct {
	*proto.Event
}

func (Event) Table() string {
	return "events"
}

func (Event) Fields() []string {
	return fields
}

func (e *Event) Values() []interface{} {
	return []interface{}{
		e.Time,
		e.Type,
		e.Browser,
		e.Os,
		e.Clicks,
	}
}
