# Golang

### Decorator
```go
package decorator

type (
	source interface {
		increment(int) int
		wrap(int) int
		proxy(int) int
		same(int) int
	}

	handler struct {
	}
)

func (handler) increment(s int) int {
	return s + 1
}

func (handler) wrap(s int) int {
	return s + 1
}

func (handler) proxy(s int) int {
	return s + 1
}

func (handler) same(s int) int {
	return s + 1
}

type (
	decorator struct {
		source
	}
)

func newDecorator(source source) source {
	return decorator{source}
}

func (d decorator) increment(s int) int {
	return d.source.increment(s) + 1
}

func (d decorator) wrap(s int) int {
	return d.source.wrap(s + 1)
}

func (d decorator) proxy(s int) int {
	return d.source.proxy(s)
}

// use d.source.same(s) as default
//func (d decorator) same(s int) int {
//	return d.source.same(s)
//}
```

```text
N = 0
Increment 300000000  4.72 ns/op  0 B/op 0 allocs/op
Wrap      300000000  4.99 ns/op  0 B/op 0 allocs/op
Proxy     300000000  4.97 ns/op  0 B/op 0 allocs/op
Same      300000000  4.78 ns/op  0 B/op 0 allocs/op

N = 127
Increment 1000000    1299 ns/op  0 B/op 0 allocs/op
Wrap      1000000    1257 ns/op  0 B/op 0 allocs/op
Proxy     1000000    1245 ns/op  0 B/op 0 allocs/op
Same      2000000     725 ns/op  0 B/op 0 allocs/op
```
##### Conclusion:
I expect when call method "proxy" or "same" with any N > 0 times decorated the duration will be like N = 0, but see overhead

### Run
```bash
go test ./... -bench=. -benchmem
````

### Extension
```go
func BenchmarkEventInlineEntity(b *testing.B) {
	items := make([]entity.Entity, 0, b.N)

	for i := 0; i < b.N; i++ {
		u := uint32(i)

		items = append(items, &proto.Event{
			Time:    u,
			Type:    u,
			Browser: u,
			Os:      u,
			Clicks:  u,
		})
	}

	b.ResetTimer()
	benchmark(items)
}

func BenchmarkEventExtractEntity(b *testing.B) {
	items := make([]entity.Entity, 0, b.N)

	for i := 0; i < b.N; i++ {
		u := uint32(i)

		items = append(items, &entity.Event{&proto.Event{
			Time:    u,
			Type:    u,
			Browser: u,
			Os:      u,
			Clicks:  u,
		}})
	}

	b.ResetTimer()
	benchmark(items)
}

func benchmark(items []entity.Entity) {
	if len(items) == 0 {
		return
	}

	item := items[0]

	_ = item.Table()
	_ = item.Fields()

	for _, item := range items {
		_ = item.Values()
	}
}
```

```text
EventInlineEntity             5000000               265 ns/op
EventExtractEntity            5000000               265 ns/op
```

##### Conclusion:
Same

### Override
```golang
type lite struct {
	a int
	b int
	c int
	d int
}

func (l *lite) set(a, b, c, d int) {
	*l = lite{
		a,
		b,
		c,
		d,
	}
}

func (l *lite) sum() int {
	c := *l

	return c.a + c.b + c.c + c.d
}
```

```golang
func Benchmark_Lite(b *testing.B) {
	l := &lite{1, 2, 4, 8}

	wg := sync.WaitGroup{}
	wg.Add(2)
	alive := true

	go func() {
		defer wg.Done()

		for alive {
			l.set(1, 2, 4, 8)
		}
	}()

	go func() {
		defer wg.Done()

		for alive {
			l.set(8, 4, 2, 1)
		}
	}()

	const expect = 15

	for i := 0; i < b.N; i++ {
		sum := l.sum()

		if expect != sum {
			b.Errorf("expect sum %d, got %d", expect, sum)
		}
	}

	alive = false

	wg.Wait()
}
```

Got:
```text
...
expect sum 15, got 6
expect sum 15, got 22
expect sum 15, got 8
expect sum 15, got 24
...
```

##### Conclusion:
Unsafe & [Is assigning a pointer atomic in Golang?](https://stackoverflow.com/questions/21447463/is-assigning-a-pointer-atomic-in-golang)

### Mutex
```golang
type ReferenceMutexCounter struct {
	mu    *sync.Mutex
	count int
}

type InlineMutexCounter struct {
	mu    sync.Mutex
	count int
}
```

##### Result
```text
BenchmarkReferenceMutexCounter      	100000000	        16.6 ns/op	       0 B/op	       0 allocs/op
BenchmarkValueMutexCounter          	100000000	        18.6 ns/op	       0 B/op	       0 allocs/op
BenchmarkInlineMutexCounter         	100000000	        18.6 ns/op	       0 B/op	       0 allocs/op
BenchmarkNewReferenceMutexCounter   	30000000	        45.9 ns/op	      24 B/op	       2 allocs/op
BenchmarkNewValueMutexCounter       	50000000	        25.3 ns/op	      16 B/op	       1 allocs/op
BenchmarkNewInlineMutexCounter      	100000000	        24.1 ns/op	      16 B/op	       1 allocs/op
```

##### Conclusion:
Diff 1 ns ~ same

### Slice race on write to different indexes
```golang
func BenchmarkSliceRace(b *testing.B) {
	data := make([]int, b.N)

	wg := new(sync.WaitGroup)
	wg.Add(b.N)

	flag := struct{}{}
	throttler := make(chan struct{}, 256)

	for i := 0; i < b.N; i++ {
		throttler <- flag
		go func(i int) {
			data[i] = i

			<-throttler

			wg.Done()
		}(i)
	}

	wg.Wait()
	close(throttler)
}
```

```bash
go test ./slicerace/... -v -bench=. -benchmem
go test ./slicerace/... -v -bench=. -benchmem -race
```

```text
BenchmarkSliceRace   	 3000000	       417 ns/op	       8 B/op	       0 allocs/op
BenchmarkSliceRace   	   30000	     58599 ns/op	       9 B/op	       0 allocs/op
```

##### try rewrite
```golang
// ...
		go func(i int) {
			data[i] = i

			<-throttler

			wg.Done()
		}(i%2)
// ...
```

```text
WARNING: DATA RACE
```
