##### Golang basic
* [A Tour of Go](https://tour.golang.org/welcome/1)
* [Go by Example](https://gobyexample.com/)
* [Введение в программирование на Go golang-book.ru](http://golang-book.ru/)
* [50 оттенков Go](https://habr.com/company/mailru/blog/314804/)
* [Эффективный Go](https://github.com/Konstantin8105/Effective_Go_RU)

##### YouTube courses
* [Программирование на Go | Технострим](https://youtu.be/9Pk7xAT_aCU)
##### YouTube channels
* [JustForFunc: Programming in Go](https://www.youtube.com/channel/UC_BzFbxG2za3bp5NRRRXJSw)
* [Gopher Academy | GopherCon 2018](https://www.youtube.com/channel/UCx9QVEApa5BKLw9r8cnOFEA/playlists)

##### O'Reilly courses
* [Ultimate Go Programming](https://www.oreilly.com/library/view/ultimate-go-programming/9780135261651/)

##### Coursera
* [Разработка веб-сервисов — основы языка](https://ru.coursera.org/learn/golang-webservices-1)
* [Разработка веб-сервисов, часть 2](https://ru.coursera.org/learn/golang-webservices-2)

### Development tools
* [A curated list of awesome Go frameworks, libraries and software](https://github.com/avelino/awesome-go)
* [Инструменты, которые помогут улучшить ваш код](https://4gophers.ru/articles/tools/#.W9LiA6deNQL/)