package mutex

import "sync"

type Counter interface {
	Add()
	Count() int
}

type ReferenceMutexCounter struct {
	mu    *sync.Mutex
	count int
}

type ValueMutexCounter struct {
	mu    sync.Mutex
	count int
}

type InlineMutexCounter struct {
	sync.Mutex
	count int
}

func NewReferenceMutexCounter() *ReferenceMutexCounter {
	return &ReferenceMutexCounter{
		mu: new(sync.Mutex),
	}
}

func (c *ReferenceMutexCounter) Add() {
	c.mu.Lock()
	c.count += 1
	c.mu.Unlock()
}

func (c *ReferenceMutexCounter) Count() int {
	c.mu.Lock()
	count := c.count
	c.mu.Unlock()

	return count
}

func NewValueMutexCounter() *ValueMutexCounter {
	return &ValueMutexCounter{}
}

func (c *ValueMutexCounter) Add() {
	c.mu.Lock()
	c.count += 1
	c.mu.Unlock()
}

func (c *ValueMutexCounter) Count() int {
	c.mu.Lock()
	count := c.count
	c.mu.Unlock()

	return count
}

func NewInlineMutexCounter() *InlineMutexCounter {
	return &InlineMutexCounter{}
}

func (c *InlineMutexCounter) Add() {
	c.Lock()
	c.count += 1
	c.Unlock()
}

func (c *InlineMutexCounter) Count() int {
	c.Lock()
	count := c.count
	c.Unlock()

	return count
}
