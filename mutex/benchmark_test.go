package mutex

import (
	"testing"
)

func BenchmarkReferenceMutexCounter(b *testing.B) {
	counter := NewReferenceMutexCounter()

	for i := 0; i < b.N; i++ {
		counter.Add()
	}
}

func BenchmarkValueMutexCounter(b *testing.B) {
	counter := NewValueMutexCounter()

	for i := 0; i < b.N; i++ {
		counter.Add()
	}
}

func BenchmarkInlineMutexCounter(b *testing.B) {
	counter := NewInlineMutexCounter()

	for i := 0; i < b.N; i++ {
		counter.Add()
	}
}

func BenchmarkNewReferenceMutexCounter(b *testing.B) {
	var counter *ReferenceMutexCounter

	for i := 0; i < b.N; i++ {
		counter = NewReferenceMutexCounter()
	}

	_ = counter
}

func BenchmarkNewValueMutexCounter(b *testing.B) {
	var counter *ValueMutexCounter

	for i := 0; i < b.N; i++ {
		counter = NewValueMutexCounter()
	}

	_ = counter
}

func BenchmarkNewInlineMutexCounter(b *testing.B) {
	var counter *InlineMutexCounter

	for i := 0; i < b.N; i++ {
		counter = NewInlineMutexCounter()
	}

	_ = counter
}
