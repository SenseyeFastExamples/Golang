package override

import (
	"sync"
	"testing"
)

func Benchmark_Lite(b *testing.B) {
	l := &lite{1, 2, 4, 8}

	wg := sync.WaitGroup{}
	wg.Add(2)
	alive := true

	go func() {
		defer wg.Done()

		for alive {
			l.set(1, 2, 4, 8)
		}
	}()

	go func() {
		defer wg.Done()

		for alive {
			l.set(8, 4, 2, 1)
		}
	}()

	const expect = 15

	for i := 0; i < b.N; i++ {
		sum := l.sum()

		if expect != sum {
			b.Errorf("expect sum %d, got %d", expect, sum)
		}
	}

	alive = false

	wg.Wait()
}
