package override

type lite struct {
	a int
	b int
	c int
	d int
}

func (l *lite) set(a, b, c, d int) {
	*l = lite{
		a,
		b,
		c,
		d,
	}
}

func (l *lite) sum() int {
	c := *l

	return c.a + c.b + c.c + c.d
}
