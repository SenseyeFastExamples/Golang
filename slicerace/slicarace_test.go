package slicerace

import (
	"sync"
	"testing"
)

func BenchmarkSliceRace(b *testing.B) {
	data := make([]int, b.N)

	wg := new(sync.WaitGroup)
	wg.Add(b.N)

	flag := struct{}{}
	throttler := make(chan struct{}, 256)

	for i := 0; i < b.N; i++ {
		throttler <- flag
		go func(i int) {
			data[i] = i

			<-throttler

			wg.Done()
		}(i)
	}

	wg.Wait()
	close(throttler)
}
