package decorator

import "testing"

const N = 127

func BenchmarkSource(b *testing.B) {
	handler := handler{}

	for i := 0; i < b.N; i++ {
		handler.increment(i)
	}
}

func BenchmarkDecoratorIncrement(b *testing.B) {
	handler := createNTimesDecoratedHandler(handler{}, N)

	for i := 0; i < b.N; i++ {
		handler.increment(i)
	}
}

func BenchmarkDecoratorWrap(b *testing.B) {
	handler := createNTimesDecoratedHandler(handler{}, N)

	for i := 0; i < b.N; i++ {
		handler.wrap(i)
	}
}

func BenchmarkDecoratorProxy(b *testing.B) {
	handler := createNTimesDecoratedHandler(handler{}, N)

	for i := 0; i < b.N; i++ {
		handler.proxy(i)
	}
}

func BenchmarkDecoratorSame(b *testing.B) {
	handler := createNTimesDecoratedHandler(handler{}, N)

	for i := 0; i < b.N; i++ {
		handler.same(i)
	}
}

func createNTimesDecoratedHandler(source source, times int) source {
	result := source

	for i := 0; i < times; i++ {
		result = newDecorator(result)
	}

	return result
}
