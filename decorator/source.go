package decorator

type (
	source interface {
		increment(int) int
		wrap(int) int
		proxy(int) int
		same(int) int
	}

	handler struct {
	}
)

func (handler) increment(s int) int {
	return s + 1
}

func (handler) wrap(s int) int {
	return s + 1
}

func (handler) proxy(s int) int {
	return s + 1
}

func (handler) same(s int) int {
	return s + 1
}
