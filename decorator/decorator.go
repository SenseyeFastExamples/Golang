package decorator

type (
	decorator struct {
		source
	}
)

func newDecorator(source source) source {
	return decorator{source}
}

func (d decorator) increment(s int) int {
	return d.source.increment(s) + 1
}

func (d decorator) wrap(s int) int {
	return d.source.wrap(s + 1)
}

func (d decorator) proxy(s int) int {
	return d.source.proxy(s)
}

// use d.source.same(s) as default
//func (d decorator) same(s int) int {
//	return d.source.same(s)
//}
